package br.ucsal.testequalidade20191.locadora;

import org.junit.Test;
import org.junit.runner.RunWith;

import br.ucsal.testequalidade20191.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20191.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20191.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20191.locadora.persistence.VeiculoDAO;

@RunWith(MockitoJUnitRunner.class)
public class LocacaoBOUnitarioTest {

	@Mock
	private ClienteDAO ClienteDAOMock;

	@Mock
	private VeiculoDAO VeiculoDAOMock;

	@Mock
	private LocacaoDAO locacaoDAOMock;
	
	@InjectMocks
	private LocacaoBO locacaoBOMock;

	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		Mockito.verify(locacaoBOMock).locarVeiculos;
	}
}
